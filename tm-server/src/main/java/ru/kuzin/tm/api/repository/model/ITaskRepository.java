package ru.kuzin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}