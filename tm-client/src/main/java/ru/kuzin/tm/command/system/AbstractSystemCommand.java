package ru.kuzin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.endpoint.ISystemEndpoint;
import ru.kuzin.tm.api.service.ICommandService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    protected ISystemEndpoint getSystemEndpoint() {
        return serviceLocator.getSystemEndpoint();
    }

}